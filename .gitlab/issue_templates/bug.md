## Expected behaviour

Doing `<a>` should result in `<b>`.

## Actual behaviour

Doing `<a>` causes error "xyz", and result is `<c>`.

## Steps to reproduce
 1. Do
 2. this
 3. and
 4. that
 
## Technical details

```ecmascript 6
... Log output ... 
```