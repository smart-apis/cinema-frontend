# build environment
FROM node:11 as builder
RUN mkdir /opt/app
WORKDIR /opt/app
ENV PATH /opt/app/node_modules/.bin:$PATH
COPY package.json /opt/app/package.json
COPY package-lock.json /opt/app/package-lock.json
RUN npm ci
COPY . /opt/app
ENV REACT_APP_API_ENTRYPOINT https://screening-events.herokuapp.com/
RUN npm run build

# production environment
FROM nginx:1.15-alpine

COPY nginx/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY nginx/nginx.conf /etc/nginx/nginx.conf

COPY --from=builder /opt/app/build /usr/share/nginx/html
CMD /bin/sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'
