image: ## build image and push to heroku registry
	heroku container:push web --app cc-cinema

release: ## release container to heroku
	heroku container:release web --app cc-cinema

deploy: image release

logs: ## show heroku logs
	heroku logs --tail --app cc-cinema
