import {frame, compact} from 'jsonld';
import {facts, getOperation} from '../client/test-client';

export default async (offerId) => {
  const offer = await compact(await frame(facts.all(), { // TODO: we definetely need framing support in hyperfact
    "@context": {},
    "@id": offerId,
    "@explicit": true
  }), {});
  const createOrder = await getOperation(
      'http://schema.org/CreateAction',
      'http://schema.org/Offer',
      'http://schema.org/Order'
  );

  const orderId = await createOrder(offer);
  console.log('ordered successfully', orderId);
}
