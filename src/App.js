import React, {Component} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';

import {HashRouter} from 'react-router-dom'
import {Route} from 'react-router'


import './App.css';
import Entrypoint from './components/Entrypoint';
import MovieTheater from './components/MovieTheater';
import ScreeningEvent from './components/EventDetails';
import Guide from './components/Guide';
import Orders from './components/Orders';
import {ResourceRoute} from './react-hydra';

import {resource} from "./react-hydra";
import {fetchAndMerge, apiEntryPoint} from "./client/test-client.js";
import NavBar from "./components/NavBar";

const CollectionRouting = (props) => {
  if (props.manages.some(it => it.property === 'type' && it.object === 'ScreeningEvent')) {
    return <Guide {...props} />
  }else if (props.manages.some(it => it.property === 'type' && it.object === 'Order')) {
    return <Orders {...props} />
  } else  {
    return `not supported collection ${JSON.stringify(props.manages)}`
  }
};

const CollectionRoute = resource(CollectionRouting,
    ['manages'], [], {
  'manages': {
    '@id': 'hydra:manages',
    '@container': '@set'
  },
  'type': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type',
  'object': {
    '@id': 'hydra:object',
    '@type': '@vocab'
  },
  'property': {
    '@id': 'hydra:property',
    '@type': '@vocab'
  },
});

class App extends Component {

  state = {
    facts: {
      '@graph': []
    }
  };

  async fetchResource(id) {
    console.log('fetch resource and merge', id);
    const oldFacts = this.state.facts;
    return fetchAndMerge(
        id, oldFacts
    ).then(facts => {
      this.setState({facts})
    }).catch(err => console.log(err));

  };



  render() {
    return (
        <React.Fragment>
          <CssBaseline />
          <div className="App">
            <HashRouter>
                <Route path="*" render={({match}) => {
                  const id = match.url === '/' ? apiEntryPoint : match.url.slice(1);

                  console.log('entering resource route', id);
                  return <ResourceRoute
                      key={id} // setting this key ensures, that a new ResourceRoute component is mounted every time the id changes
                      context={{
                        '@vocab': 'http://schema.org/',
                        "hydra": "http://www.w3.org/ns/hydra/core#",
                        "title": "hydra:title",
                      }}
                      id={id}
                      navBar={({title, name}) => <NavBar title={ title || name }/>}
                      typeRoutes={
                        {
                          Entrypoint,
                          MovieTheater,
                          ScreeningEvent,
                          "hydra:Collection": CollectionRoute,
                        }
                      }
                      fetchResource={(id) => this.fetchResource(id)}
                  />
                }
                }/>
            </HashRouter>
          </div>
        </React.Fragment>
    );
  }
}

export default App;
