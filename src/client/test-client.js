import {frame} from 'jsonld';
import createStore from 'hyperfact';

export const apiEntryPoint = process.env.REACT_APP_API_ENTRYPOINT || 'http://screening-events.test/';


export const facts = createStore();

const fetched = [];

export const fetchAndMerge = async (id) => {
  if (!id) {
    throw Error('cannot fetch undefined id');
  }

  try {
    if (!fetched.includes(id)) {
      console.log('need to fetch:', id, 'already fetched:', fetched);
      fetched.push(id);

      const response = await fetch(id);
      const resource = await response.json();
      const newFacts = await facts.merge(resource);
      console.log('current knowledge, after fetching', {resource}, 'from', id, newFacts.all());
      return newFacts
    }

  } catch (err) {
    console.log(err);
  }

  return Promise.resolve()
};

export const getOperation = async (type, expects, returns) => {
  const operation = await frameOperation(type, expects, returns);
  console.log('framed operation', operation);
  return (body) => operationAndMerge(operation, body);
};

const frameOperation = async (type, expects, returns) => {
  const operationFrame = {
    "@context": {
      "@vocab": "http://www.w3.org/ns/hydra/core#",
      "id": "@id",
      "type": "@type",
      "expects": {
        "@id": "expects",
        "@type": "@vocab"
      },
      "returns": {
        "@id": "returns",
        "@type": "@vocab"
      },
      "operationTarget": {
        "@reverse": "operation"
      }
    },
    "type": type,
    "expects": expects,
    "returns": returns,
    "operationTarget": {}
  };
  console.log('operation framing', facts.all(), operationFrame);
  const operationMatches = await frame(facts.all(),
      operationFrame);
  console.log('operation framing result', operationMatches);

  const match = operationMatches["@graph"][0];
  if (!match) {
    throw new Error(
        `no operation known, that expects ${expects} and returns ${returns}`)
  }
  return match;
};

export const operationAndMerge = async (operation, body) => {
  console.log('performing operation', operation, body);
  const response = await fetch(operation.operationTarget.id, {
    method: operation.method,
    headers: {
      "Content-Type": "application/ld+json",
      "Accept": "application/ld+json"
    },
    body: JSON.stringify(body)
  });
  const resource = await response.json();
  await facts.merge(resource);
  return response.headers.get('Location')
};

