import React, {Component} from 'react';
import Drawer from '@material-ui/core/Drawer';
import Button from "@material-ui/core/Button";
import ReactJson from "react-json-view";
import {compact, frame} from "jsonld";

import IconButton from '@material-ui/core/IconButton';
import InspectIcon from '@material-ui/icons/Search';

import styles from './HydraDevTools.module.css'
import Grid from "@material-ui/core/Grid";

const context = {
  'hydra': 'http://www.w3.org/ns/hydra/core#',
  'id': '@id',
  'collections': {
    '@id': 'hydra:collection',
    '@type': '@id',
    '@container': '@set'
  },
  'method': 'hydra:method',
  'label': 'http://www.w3.org/2000/01/rdf-schema#label',
  'operations': {
    '@id': 'hydra:operation',
    '@container': '@set'
  }
};

export class HydraDevTools extends Component {

  state = {
      show: false,
  };

  async updateFacts(oldFacts) {
    const contexts = [context, this.props.context];
    const graph = await frame(oldFacts, {
      '@context': contexts,
      'id': this.props.id,
      '@embed': '@always'
    });

    const facts = await compact(graph, contexts);
    this.setState({facts})
  }


  async componentDidMount() {
    this.updateFacts(this.props.facts);
  }


  async componentWillReceiveProps(nextProps) {
    this.updateFacts(nextProps.facts);
  }

  toggleDrawer = (open) => () => {
    this.setState({
      show: open,
    });
  };

  render() {
    const {facts} = this.state;
    if (!facts) return null;


    const {fetchResource, id} = this.props;

    const { collections } = facts;

    return <React.Fragment>
      <IconButton style={{position:'absolute'}} className={styles.inspectButton} onClick={this.toggleDrawer(true)} aria-label="Inspect">
        <InspectIcon />
      </IconButton>
      <Drawer
          open={this.state.show}
          onClose={this.toggleDrawer(false)}
          className={styles.drawer}
          classes={{
            paper: styles.drawerPaper,
          }}
          anchor="right"
      >
      <Grid container spacing={24} style={{padding: 24}}>
        <div>
      <h2>Hydra Dev Tools</h2>
          <h3>{id}</h3>
      <h3>Collections</h3>
      {
        collections ? collections.map(it => <div key={it.id}><h4>{it.title}</h4>
          <div>
            <Button variant="contained" onClick={() => fetchResource(it.id)}>
              {it.operations.find(op => op.method === 'GET').label}
            </Button></div>
        </div>) : 'none'
      }
      <h3>Raw JSON-LD</h3>
      <ReactJson src={facts} collapsed={1} name={false}/>
        </div>
      </Grid>
    </Drawer>
    </React.Fragment>
  }
}