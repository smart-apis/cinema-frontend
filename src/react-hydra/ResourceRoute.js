import React, {Component} from "react";
import {HydraDevTools} from "./HydraDevTools";

import {resource} from '../react-hydra/resource'

const defaultContext = {
  'title': 'hydra:title'
};
class ResourceRoute extends Component {

  render() {
    console.log('render resource', this.props.id, {...this.props});
    const {id, fetchResource, typeRoutes} = this.props;
    console.log('searching component for type', this.props.type, typeRoutes);
    const Component = typeRoutes[this.props.type] || (() => <HydraDevTools id={id} fetchResource={fetchResource} context={defaultContext} facts={this.props.facts} />);
    console.log('matching component', this.props.type, Component.displayName);
    const NavBar = this.props.navBar;
    return (
        <React.Fragment>
          <NavBar {...this.props}/>
          <Component id={id} fetchResource={fetchResource} facts={this.props.facts}/>
        </React.Fragment>
    );
  }
}

export default resource(ResourceRoute,  ['id', 'type'], ['title', 'name'], defaultContext)
