import {provide} from "hyperprops";

import hyperfetch from 'hyperfetch';

import {facts} from '../client/test-client';


const provider = {
  get: (id) => facts.getResource(id),
  fetch: (id) => hyperfetch(id).then(resource => facts.merge(resource)),
  subscribe: (id, handler) => facts.subscribe(id, handler),
  unsubscribe: (id, handler) => facts.unsubscribe(id, handler)
};


export const resource = provide(
    // provide a base context, used by all components
    // this is good to declare common prefixes or often used properties
    {
      'id': '@id',
      'type': '@type',
      '@vocab': 'http://schema.org/',
      'hydra': 'http://www.w3.org/ns/hydra/core#',
      'xsd': 'http://www.w3.org/2001/XMLSchema#'
    },
    provider
);
