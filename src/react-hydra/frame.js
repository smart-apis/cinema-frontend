import {Component} from "react";
import {frame} from "jsonld";
import React from "react";
import {HydraDevTools} from "./HydraDevTools";
import LinearProgress from '@material-ui/core/LinearProgress';

import {facts} from '../client/test-client';

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName ||
      WrappedComponent.name || 'Component'
}

// TODO frameing should be supported by hyperfact
// TODO unifiy code duplications with `connect`
export default (WrappedComponent,
    getFrame, options = { debug: false, subscribe: 'id' }) => class ResourceContainer extends Component {

  state = {
    initial: true,
  };

  updateFacts = async () => {
    const allFacts = facts.all();
    let componentFrame = getFrame(this.props);
    console.log('frame', {facts: JSON.stringify(allFacts), frame: JSON.stringify(componentFrame)});
    const finalFacts = await frame(allFacts, componentFrame);
    console.log('final facts after framing', finalFacts);
    this.setState({facts: finalFacts["@graph"]})
  };

  static displayName = `Resource(${getDisplayName(WrappedComponent)})`;


  async componentDidMount() {
    if (!this.props[options.subscribe]) {
      throw new Error(`${ResourceContainer.displayName} does need a property "${options.subscribe}"`)
    }
    this.props.fetchResource(this.props[options.subscribe]);

    facts.subscribe(this.props[options.subscribe], this.updateFacts);

    this.updateFacts();
  }

  render() {
    console.log('render', ResourceContainer.displayName, {state: this.state,
      props: this.props});
    const {facts} = this.state;
    const {id, fetchResource} = this.props;
    if (!facts) {
      return <LinearProgress />
    }
    return <React.Fragment>
      {options.debug ? <HydraDevTools id={id} fetchResource={fetchResource}
                                      context={getFrame} /> : null}
      <WrappedComponent {...this.props} resources={facts} fetchResource={fetchResource} />
    </React.Fragment>
  }
};