import React from 'react';

import {frame} from '../react-hydra';

import Grid from '@material-ui/core/Grid';
import Movie from './Movie';

const Movies = ({ resources }) =>
  <Grid container item xs={12} spacing={16}>
      {resources.map(
          it => <Grid item md={4} sm={6} xs={12}><Movie key={it.id} {...it} /></Grid>
      )}
  </Grid>;

export default frame(Movies, ({guide}) => ({
  "@context": {
    '@vocab': 'http://schema.org/',
    'id': '@id',
    'type': '@type',
    "memberOf": {
      "@reverse": "member",
      "@container": "@set"
    },
    "events": {
      "@reverse": "workPresented",
      "@container": "@set"
    }
  },
  "type": "Movie",
  "events": {
    "memberOf": {
      "id": guide,
      "@explicit": true
    }
  }
}), { subscribe: 'guide' });
