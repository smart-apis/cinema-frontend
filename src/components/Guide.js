import React from 'react';
import Grid from '@material-ui/core/Grid';
import Movies from './Movies';

const Guide = ({id, fetchResource }) => <div>
  <Grid container spacing={24} style={{padding: 24}}>
    <Grid item xs={12}>
      <Movies guide={id} fetchResource={fetchResource} />
    </Grid>
  </Grid>
</div>;

export default Guide;