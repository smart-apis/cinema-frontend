import React from 'react';
import Grid from '@material-ui/core/Grid';

import {Link} from 'react-router-dom'

import {resource} from '../react-hydra/resource';

const Entrypoint = ({id, title, collections }) => <div>
  <Grid container spacing={24} style={{padding: 24}}>
    <Grid item xs={12} sm={6} lg={4} xl={3}>
      <ul>
        {collections.map(it => <li key={it.id}><Link to={`/${it.id}`}>{it.title}</Link></li>)}
      </ul>
    </Grid>
  </Grid>
</div>;

export default resource(Entrypoint,
    ['title', 'collections'], [],
    {
      'title': 'hydra:title',
      'collections': {
        '@id': 'hydra:collection',
        '@container': '@set'
      }
    }
);