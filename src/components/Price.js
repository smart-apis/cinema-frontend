import {resource} from "../react-hydra";

const Price = ({price, priceCurrency}) => `${price} ${priceCurrency}`;

export default resource(Price, ['price', 'priceCurrency'], [], {
  "price": {
    "@type": "xsd:integer"
  }
});
