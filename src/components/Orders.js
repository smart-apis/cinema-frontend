import React from 'react';
import Grid from '@material-ui/core/Grid';

import {resource} from '../react-hydra';
import Order from "./orders/Order";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";

const Orders = ({orders = [] }) => <div>
  <Grid container spacing={24} style={{padding: 24}}>
    <Grid item xs={12}>
      <Table>
        <TableBody>
      {orders.map(order => <Order key={order.id} id={order.id} />)}
        </TableBody>
      </Table>
    </Grid>
  </Grid>
</div>;

export default resource(Orders, [], ['orders'], {
  "title": "hydra:title",
  "orders": {
    "@id": "hydra:member",
    "@container": "@set"
  }
});
