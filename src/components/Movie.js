import React from "react";
import Event from "./Event";
import {resource} from "../react-hydra";
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Card from "@material-ui/core/Card";
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

const styles = theme => ({
  card: {
    margin: 10,
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
});

const Movie = withStyles(styles)(({events = [], name, classes}) =>
    <Card className={classes.card}>
      <CardHeader title={name} />
      <CardMedia
          className={classes.media}
          image={`https://lorempixel.com/800/600/abstract/${name}`}
          title={name}
      />
      <CardContent>
        <section>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et</section>
        <List>
          {events.map(event =>
              <Event key={event.id} id={event.id} />
          )}
        </List>
      </CardContent>
    </Card>);

export default resource(Movie, ['name']);
