import React from 'react';

import {resource} from "../react-hydra";
import Date from "./Date";
import Grid from "@material-ui/core/Grid";
import Movie from "./Movie";
import LocationName from "./LocationName";
import Offer from "./Offer";

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

const EventDetails = ({id, startDate, workPresented, location, offers}) => (
      <Grid container spacing={24} style={{padding: 24}}>
        <Grid item xs={6}>
          <Movie id={workPresented.id} />
        </Grid>
        <Grid item xs={6}>
          <h3>Details</h3>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell component="th" align={"right"} scope="row">
                  When?
                </TableCell>
                <TableCell align="left"><Date value={startDate}/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" align={"right"} scope="row">
                  Where?
                </TableCell>
                <TableCell align="left"><LocationName link id={location.id} /></TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <h3>Ticket offers</h3>
          <Table>
            <TableBody>
              {offers.map(offer => <Offer key={offer.id} id={offer.id} />)}
            </TableBody>
          </Table>


        </Grid>
      </Grid>
);

export default resource(EventDetails,
    ['startDate', 'workPresented', 'location', 'offers'], [], {
      'offers': {
        '@container': '@set'
      }
    }
);
