import React from 'react';
import moment from "moment";

const format = 'dd, DD.MM - hh:mm';

export default ({value}) => <span>{moment(value).format(format)}</span>