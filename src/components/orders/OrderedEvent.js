import React from "react";
import {resource} from "../../react-hydra";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Date from "../Date";
import LocationName from "../LocationName";

const OrderedEvent = ({startDate, workPresented, location}) =>
    <TableRow>
      <TableCell component="th" align={"left"} scope="row">
        You ordered
      </TableCell>
      <TableCell align="left">{workPresented.name}, <Date value={startDate}/>,
          <LocationName id={location.id} />
      </TableCell>
      <TableCell/>
    </TableRow>;

export default resource(OrderedEvent, ['startDate', 'workPresented', 'location']);