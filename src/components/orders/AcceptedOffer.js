import React from 'react';
import {resource} from "../../react-hydra";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import order from '../../actions/order';
import OrderedEvent from "./OrderedEvent";
import Price from "../Price";

const AcceptedOffer = ({id, name='Standard ticket', offeredItem, priceSpecification}) => (
    <React.Fragment>
      {offeredItem ? <OrderedEvent id={offeredItem.id} /> : null}
      <TableRow>
        <TableCell component="th" align={"left"} scope="row">
          {name}
        </TableCell>
        <TableCell
            align="left"><Price id={priceSpecification.id}/></TableCell>
        <TableCell align="left"><Button color={"primary"} variant="contained"
                                        onClick={() => order(
                                            id)}>+1</Button></TableCell>
      </TableRow>
    </React.Fragment>
);

export default resource(AcceptedOffer, ['offeredItem', 'priceSpecification'], ['name']);
