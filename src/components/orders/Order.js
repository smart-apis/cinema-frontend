import React from "react";
import {resource} from "../../react-hydra";
import AcceptedOffer from "./AcceptedOffer";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Date from "../Date";

const Order = ({orderDate, acceptedOffer}) =>
    <React.Fragment>
      <AcceptedOffer id={acceptedOffer.id} orderButtonLabel="+1"/>
      <TableRow>
        <TableCell component="th" align={"left"} scope="row">
          Order date
        </TableCell>
        <TableCell align="left"><Date value={orderDate}/></TableCell>
        <TableCell/>
      </TableRow>
    </React.Fragment>;


export default resource(Order, ['orderDate', 'acceptedOffer']);