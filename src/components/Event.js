import React from "react";
import {Link} from 'react-router-dom'
import {resource} from "../react-hydra";

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import LocationName from './LocationName'

import Date from './Date'

const Event = ({id, startDate, location}) =>
    <ListItem button component={Link} to={`/${id}`}>
      <ListItemText
          primary={<Date value={startDate}/>}
          secondary={<LocationName id={location.id}/>}
      />

    </ListItem>;

export default resource(Event, ['startDate', 'location']);