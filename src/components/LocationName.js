import React from "react";
import {Link} from 'react-router-dom'
import {resource} from "../react-hydra";

const placeholder = <div style={{
  backgroundColor: '#d4cbcb',
  borderRadius: 10,
  width: 150,
  height: '1rem',
  position: 'relative',
  display: 'inline-block'
}}>&nbsp;</div>;

const LocationName = ({id, name, link}) => <span>{name ? ( link ? <Link to={`/${id}`}>{name}</Link> : `${name}`) : placeholder}</span>;

export default resource(LocationName, ['name']);