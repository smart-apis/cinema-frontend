import React from 'react';
import Grid from '@material-ui/core/Grid';

import {resource} from '../react-hydra';

import EventTeaser from './EventTeaser';

import moment from 'moment';

const MovieTheater = ({id, name, events }) => <div>
  <Grid container spacing={24} style={{padding: 24}}>
    <Grid item xs={12} sm={6} lg={4} xl={3}>

      <h2>Events</h2>
      {events.sort((a,b) => moment(a.startDate) - moment(b.startDate)).map(
          it =>
              <EventTeaser key={it.id} id={it.id} />
      )}

    </Grid>
  </Grid>
</div>;

export default resource(MovieTheater,
    ['name', 'events'], [],
    {
      'events': {
        '@id': 'event',
        '@container': '@set'
      }
    });