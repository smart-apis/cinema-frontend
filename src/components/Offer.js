import React from 'react';
import {resource} from "../react-hydra";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import order from '../actions/order';
import Price from './Price';

const Offer = ({id, name = 'Standard ticket', orderButtonLabel = "Add to cart", priceSpecification}) => (
    <TableRow>
      <TableCell component="th" align={"left"} scope="row">
        {name}
      </TableCell>
      <TableCell align="left"><Price id={priceSpecification.id} /></TableCell>
      <TableCell align="left"><Button color={"primary"} variant="contained" onClick={() => order(id)}>{orderButtonLabel}</Button></TableCell>
    </TableRow>
);

export default resource(Offer, ['priceSpecification'], ['name']);
