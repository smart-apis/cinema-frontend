import React from "react";
import {resource} from "../react-hydra";

import Date from "./Date";

const EventTeaser = ({id, workPresented, startDate }) =>
    <div>
      <h3><Date value={startDate}/></h3>
      <section>"{workPresented.name}"</section>
    </div>;

export default resource(EventTeaser, ['workPresented', 'startDate']);